# Run Program.cs to "play".
Feel free to play around with the xUnit tests.
The classes are poorly encapsulated as this was just to play around so don't use it as an example ^^

## What primary stat do the characters scale on?
- Mages scale based on intelligence.
- Rogues scale based on dexterity.
- Rangers scale based on dexterity.
- Warriors scale based on strength.

## What armor types can classes wear?
- Mages can wear cloth.
- Rogues can wear leather and mail.
- Rangers can wear leather and mail.
- Warriors can wear mail and plate.

## What weapons can classes use?
- Mages can use staffs and wands.
- Rogues can use daggers and swords.
- Rangers can use bows.
- Warriors can use axes, hammers and swords.

---
**The class diagram can be found in the RPGCharacters project.**
