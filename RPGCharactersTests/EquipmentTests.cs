﻿using RPGCharacters.Hero.Classes;
using RPGCharacters.Items;
using RPGCharacters.Items.Exceptions;
using RPGCharacters.Types;

namespace RPGCharactersTests
{
    public class EquipmentTests
    {
        #region Create Items

        [Fact]
        public void CreateItem_CreateWeapon_ShouldReturnWeaponName()
        {
            var testAxe = new Weapon()
            {
                Name = "Common Axe",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Weapon,
                WeaponType = Weapon.WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() {Damage = 1, AttackSpeed = 1.1}
            };
            var expected = "Common Axe";
            var actual = testAxe.Name;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateItem_CreateWeapon_ShouldReturnWeaponDPS()
        {
            var testBow = new Weapon()
            {
                Name = "Common Bow",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Weapon,
                WeaponType = Weapon.WeaponTypes.Bow,
                WeaponAttributes = new WeaponAttributes() {Damage = 12, AttackSpeed = 0.8}
            };
            var expected = 12*0.8;
            var actual = testBow.GetWeaponDamage();
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateItem_CreateArmor_ShouldReturnArmorName()
        {
            var testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Body,
                ArmorType = Armor.ArmorTypes.Plate,
                ItemAttributes = new PrimaryAttributes() { Intelligence = 5 }
            };
            var expected = "Common plate body armor";
            var actual = testPlateBody.Name;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateItem_CreateArmor_ShouldReturnArmorAttributes()
        {
            var testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Body,
                ArmorType = Armor.ArmorTypes.Plate,
                ItemAttributes = new PrimaryAttributes() { Intelligence = 5 }
            };
            var expected = new []{0,0,5};
            var actual = new []{ testPlateBody.ItemAttributes.Strength, testPlateBody.ItemAttributes.Dexterity, testPlateBody.ItemAttributes.Intelligence};
            Assert.Equal(expected, actual);
        }

        
        #endregion
        
        #region Equip Items

        [Fact]
        public void EquipItem_EquipWeaponInFreeSlot_ShouldReturnSuccessString()
        {
            var weapon = new Weapon()
            {
                Name = "Common Axe",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Weapon,
                WeaponType = Weapon.WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() {Damage = 7, AttackSpeed = 1.1}
            };
            var hero = new Warrior("HeroName");
            var expected = "New weapon equipped!";
            var actual = hero.EquipWeapon(weapon);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipItem_EquipWeaponInTakenSlot_ShouldReturnSuccessString()
        {
            var firstWeapon = new Weapon()
            {
                Name = "Bad Common Axe",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Weapon,
                WeaponType = Weapon.WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() {Damage = 1, AttackSpeed = 0.5}
            };
            var secondWeapon = new Weapon()
            {
                Name = "Common Axe",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Weapon,
                WeaponType = Weapon.WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() {Damage = 7, AttackSpeed = 1.1}
            };
            var hero = new Warrior("HeroName");
            hero.EquipWeapon(firstWeapon);
            var expected = "New weapon equipped!";
            var actual = hero.EquipWeapon(secondWeapon);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipItem_EquipArmorInFreeSlot_ShouldReturnSuccessString()
        {
            var armor = new Armor()
            {
                Name = "Common plate body armor",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Body,
                ArmorType = Armor.ArmorTypes.Plate,
                ItemAttributes = new PrimaryAttributes() { Intelligence = 5 }
            };
            var hero = new Warrior("HeroName");
            var expected = "New armor equipped!";
            var actual = hero.EquipArmor(armor);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipItem_EquipArmorInTakenSlot_ShouldReturnSuccessString()
        {
            var firstArmor = new Armor()
            {
                Name = "Bad plate body armor",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Body,
                ArmorType = Armor.ArmorTypes.Plate,
                ItemAttributes = new PrimaryAttributes() { Intelligence = 2 }
            };
            var secondArmor = new Armor()
            {
                Name = "Common plate body armor",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Body,
                ArmorType = Armor.ArmorTypes.Plate,
                ItemAttributes = new PrimaryAttributes() { Intelligence = 5 }
            };
            var hero = new Warrior("HeroName");
            hero.EquipArmor(firstArmor);
            var expected = "New armor equipped!";
            var actual = hero.EquipArmor(secondArmor);
            Assert.Equal(expected, actual);
        }

        #endregion

        #region Invalid Equip Items
        
        [Fact]
        public void EquipItem_EquipWeaponWithTooHighLevelRequirement_ShouldReturnInvalidWeaponException()
        {
            var expected = new Weapon()
            {
                Name = "Iron Axe",
                RequiredLevel = 3,
                ItemSlot = Item.ItemSlots.Weapon,
                WeaponType = Weapon.WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() {Damage = 3, AttackSpeed = 1.1}
            };
            var hero = new Warrior("HeroName");
            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(expected));
        }

        [Fact]
        public void EquipItem_EquipArmorWithTooHighLevelRequirement_ShouldReturnInvalidArmorException()
        {
            var expected = new Armor()
            {
                Name = "Iron plate body armor",
                RequiredLevel = 3,
                ItemSlot = Item.ItemSlots.Body,
                ArmorType = Armor.ArmorTypes.Plate,
                ItemAttributes = new PrimaryAttributes() { Strength = 5, Intelligence = 5}
            };
            var hero = new Warrior("HeroName");
            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(expected));
        }
        
        [Fact]
        public void EquipItem_EquipWeaponWithWrongWeaponType_ShouldReturnInvalidWeaponException()
        {
            var expected = new Weapon()
            {
                Name = "Common Axe",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Weapon,
                WeaponType = Weapon.WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() {Damage = 1, AttackSpeed = 1.1}
            };
            
            var hero = new Mage("HeroName");

            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(expected));
        }

        [Fact]
        public void EquipItem_EquipArmorWithWrongWeaponType_ShouldReturnInvalidArmorException()
        {
            var expected = new Armor()
            {
                Name = "Common plate body armor",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Body,
                ArmorType = Armor.ArmorTypes.Plate,
                ItemAttributes = new PrimaryAttributes() { Intelligence = 5}
            };
            
            var hero = new Mage("HeroName");

            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(expected));
        }


        #endregion

        #region Calculate Damage

        [Fact]
        public void CalculateDamage_NoWeaponEquipped_ShouldReturnCorrectDamage()
        {
            var hero = new Warrior("HeroName");
            var expected = 1 * (1 + (5 / 100));
            var actual = hero.CalculateDamage();
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CalculateDamage_WeaponEquipped_ShouldReturnCorrectDamage()
        {
            var hero = new Warrior("HeroName");
            var testAxe = new Weapon()
            {
                Name = "Common Axe",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Weapon,
                WeaponType = Weapon.WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() {Damage = 7, AttackSpeed = 1.1}
            };
            hero.EquipWeapon(testAxe);
            var expected = (7 * 1.1) * (1 + (5 / 100));
            var actual = hero.CalculateDamage();
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CalculateDamage_WeaponAndArmorEquipped_ShouldReturnCorrectDamage()
        {
            var hero = new Warrior("HeroName");
            var testAxe = new Weapon()
            {
                Name = "Common Axe",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Weapon,
                WeaponType = Weapon.WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() {Damage = 7, AttackSpeed = 1.1}
            };
            var testArmor = new Armor()
            {
                Name = "Common plate body armor",
                RequiredLevel = 1,
                ItemSlot = Item.ItemSlots.Body,
                ArmorType = Armor.ArmorTypes.Plate,
                ItemAttributes = new PrimaryAttributes() { Intelligence = 5 }
            };
            hero.EquipWeapon(testAxe);
            hero.EquipArmor(testArmor);
            var expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
            var actual = hero.CalculateDamage();
            Assert.Equal(expected, actual);

        }

        #endregion
    }
}
