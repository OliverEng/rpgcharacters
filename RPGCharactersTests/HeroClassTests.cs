using RPGCharacters.Hero.Classes;
using RPGCharacters.Items;
using RPGCharacters.Items.Exceptions;
using RPGCharacters.Types;

namespace RPGCharactersTests
{
    public class HeroClassTests
    {
        #region Creation

        [Fact]
        public void Constructor_InitializeHero_ShouldReturnLevel()
        {
            var expected = 1;
            var hero = new Mage("HeroName");
            var actual = hero.GetLevel();
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("Mage", new[] { 1, 1, 8 })]
        [InlineData("Ranger", new[] { 1, 7, 1 })]
        [InlineData("Rogue", new[] { 2, 6, 1 })]
        [InlineData("Warrior", new[] { 5, 2, 1 })]
        public void Constructor_InitializeHero_ShouldReturnCorrectHeroClassStats(string heroClass, int[] expected)
        {
            switch (heroClass)
            {
                case "Mage":
                    var mage = new Mage("Mage");
                    Assert.Equal(expected, new[] { mage.GetStats().Strength, mage.GetStats().Dexterity, mage.GetStats().Intelligence });
                    break;
                case "Ranger":
                    var ranger = new Ranger("Ranger");
                    Assert.Equal(expected, new[] { ranger.GetStats().Strength, ranger.GetStats().Dexterity, ranger.GetStats().Intelligence });
                    break;
                case "Rogue":
                    var rogue = new Rogue("Rogue");
                    Assert.Equal(expected, new[] { rogue.GetStats().Strength, rogue.GetStats().Dexterity, rogue.GetStats().Intelligence });
                    break;
                case "Warrior":
                    var warrior = new Warrior("Warrior");
                    Assert.Equal(expected, new[] { warrior.GetStats().Strength, warrior.GetStats().Dexterity, warrior.GetStats().Intelligence });
                    break;
            }


        }

        #endregion

        #region LevelUp

        [Fact]
        public void LevelUp_LevelUpHero_ShouldReturnNewLevel()
        {
            var expected = 2;
            var hero = new Mage("HeroName");
            hero.LevelUp();
            var actual = hero.GetLevel();
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("Mage", new[] { 1 + 1, 1 + 1, 8 + 5 })]
        [InlineData("Ranger", new[] { 1 + 1, 7 + 5, 1 + 1 })]
        [InlineData("Rogue", new[] { 2 + 1, 6 + 4, 1 + 1})]
        [InlineData("Warrior", new[] { 5 + 3, 2 + 2, 1 + 1 })]
        public void LevelUp_LevelUpHero_ShouldReturnCorrectHeroClassStatsAfterLevelUp(string heroClass, int[] expected)
        {
            switch (heroClass)
            {
                case "Mage":
                    var mage = new Mage("Mage");
                    mage.LevelUp();
                    Assert.Equal(expected, new[] { mage.GetStats().Strength, mage.GetStats().Dexterity, mage.GetStats().Intelligence });
                    break;
                case "Ranger":
                    var ranger = new Ranger("Ranger");
                    ranger.LevelUp();
                    Assert.Equal(expected, new[] { ranger.GetStats().Strength, ranger.GetStats().Dexterity, ranger.GetStats().Intelligence });
                    break;
                case "Rogue":
                    var rogue = new Rogue("Rogue");
                    rogue.LevelUp();
                    Assert.Equal(expected, new[] { rogue.GetStats().Strength, rogue.GetStats().Dexterity, rogue.GetStats().Intelligence });
                    break;
                case "Warrior":
                    var warrior = new Warrior("Warrior");
                    warrior.LevelUp();
                    Assert.Equal(expected, new []{warrior.GetStats().Strength, warrior.GetStats().Dexterity, warrior.GetStats().Intelligence});
                    break;
            }
        }
        #endregion

    }
}