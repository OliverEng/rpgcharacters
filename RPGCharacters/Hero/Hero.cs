﻿using RPGCharacters.Items;
using RPGCharacters.Items.Exceptions;
using RPGCharacters.Types;

namespace RPGCharacters.Hero
{
    public abstract class Hero
    {
        // Properties
        public string Name { get; }

        private int Level { get; set; }
        public Dictionary<Item.ItemSlots, Item?> EquippedItems { get; set; }

        private readonly PrimaryAttributes _baseStats = new PrimaryAttributes()
        {
            Strength = 1,
            Dexterity = 1,
            Intelligence = 1
        };

        private PrimaryAttributes _totalStats = new PrimaryAttributes();

        // Constructor
        protected Hero(string name)
        {
            Name = name;
            Level = 1;
            EquippedItems = new Dictionary<Item.ItemSlots, Item?>();
        }

        /// <summary></summary>
        /// <returns>Current Hero Level as int.</returns>
        public int GetLevel()
        {
            return Level;
        }

        /// <summary></summary>
        /// <returns>Returns the current base attributes of the hero as PrimaryAttributes type.</returns>
        public PrimaryAttributes GetStats()
        {
            return _baseStats;
        }

        /// <summary>
        /// Adds the given values to the base stats of the hero.
        /// </summary>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        public void AddStats(int strength, int dexterity, int intelligence)
        {
            _baseStats.Strength += strength;
            _baseStats.Dexterity += dexterity;
            _baseStats.Intelligence += intelligence;
        }

        /// <summary>
        /// Sets the base stats of the hero to the given values.
        /// </summary>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        public void SetStats(int strength, int dexterity, int intelligence)
        {
            _baseStats.Strength = strength;
            _baseStats.Dexterity = dexterity;
            _baseStats.Intelligence = intelligence;
        }
        /// <summary>
        /// Increases the hero level by one and adds class appropriate stats.
        /// </summary>
        public virtual void LevelUp()
        {
            Level++;
        }

        /// <summary>
        /// Checks if the hero is sufficient level to equip the armor, and if class can equip armor type. then equips it in slot.
        /// If the slot is already occupied, the armor is swapped with the new item in the slot.
        /// </summary>
        /// <param name="armorToEquip"></param>
        /// <returns>Success message.</returns>
        /// <exception cref="InvalidArmorException"></exception>
        public virtual string EquipArmor(Armor? armorToEquip)
        {
            if (armorToEquip.RequiredLevel > Level)
            {
                throw new InvalidArmorException("You are not high enough level to equip this armor.");
            }
            if (EquippedItems.ContainsKey(armorToEquip.ItemSlot))
            {
                EquippedItems[armorToEquip.ItemSlot] = armorToEquip;
            }
            else
            {
                EquippedItems.Add(armorToEquip.ItemSlot, armorToEquip);
            }

            return "New armor equipped!";
        }
        /// <summary>
        /// Checks if the hero is sufficient level to equip the weapon, and if class can equip weapon type. Then equips it in slot.
        /// If the slot is already occupied, the weapon is swapped with the new weapon in the slot.
        /// </summary>
        /// <param name="weaponToEquip"></param>
        /// <returns>Success message.</returns>
        /// <exception cref="InvalidWeaponException"></exception>
        public virtual string EquipWeapon(Weapon? weaponToEquip)
        {
            if (weaponToEquip.RequiredLevel > Level)
            {
                throw new InvalidWeaponException("You are not high enough level to equip this weapon.");
            }
            if (EquippedItems.ContainsKey(weaponToEquip.ItemSlot))
            {
                EquippedItems[weaponToEquip.ItemSlot] = weaponToEquip;
            }
            else
            {
                EquippedItems.Add(weaponToEquip.ItemSlot, weaponToEquip);
            }

            return "New weapon equipped!";
        }

        /// <summary>
        /// Calculates the total stats of the hero by adding the base stats and the stats of the equipped items.
        /// </summary>
        /// <returns>The calculated total stats.</returns>
        public PrimaryAttributes CalculateStats()
        {
            _totalStats = _baseStats;
            foreach (var item in EquippedItems.Where(item => item.Value.ItemAttributes != null))
            {
                _totalStats += item.Value.ItemAttributes;
            }

            return _totalStats;
        }

        /// <summary>
        /// Calculates the total damage output of the hero.
        /// Takes into account DPS of equipped weapon and primary stat of class after calculating total stats.
        /// </summary>
        /// <returns>Total damage as double</returns>
        public virtual double CalculateDamage()
        {
            var totalStats = CalculateStats();
            if (!EquippedItems.ContainsKey(Item.ItemSlots.Weapon))
            {
                return 1;
            }
            var weaponDamage = EquippedItems[Item.ItemSlots.Weapon].GetWeaponDamage();
            return weaponDamage;
        }

        /// <summary>
        /// Convert character information into a list of strings
        /// </summary>
        /// <param name="className"></param>
        /// <returns>string list with character information</returns>
        public List<string> DisplayInformation(string className)
        {
            CalculateStats();
            var returnList = new List<string>
            {
                $"Name: {Name}",
                $"Class: {className}",
                $"Level: {Level}",
                $"Base Attributes:\n Strength:{_baseStats.Strength} Dexterity: {_baseStats.Dexterity} Intelligence: {_baseStats.Intelligence}",
                $"Total Attributes:\n Strength:{_totalStats.Strength} Dexterity: {_totalStats.Dexterity} Intelligence: {_totalStats.Intelligence}",
                $"Damage: {CalculateDamage()}"
            };
            return returnList;
        }

        /// <summary>
        /// Convert equipped item list into string list with item information
        /// </summary>
        /// <param name="className"></param>
        /// <returns>string list with equipped item information</returns>
        public List<string> DisplayItemInformation()
        {
            var returnList = new List<string>();
            foreach (var item in EquippedItems)
            {
                if (item.Key == Item.ItemSlots.Weapon)
                {
                    returnList.Add($"{item.Key}:\n  Name: {item.Value.Name}\n  Weapon Type: {item.Value.GetWeaponTypes().ToString()}\n  " + 
                                   $"Level Req: {item.Value.RequiredLevel}\n  Damage: {item.Value.WeaponAttributes!.Damage}\n  " +
                                   $"Atk Speed: {item.Value.WeaponAttributes.AttackSpeed}");
                }
                else
                {
                    returnList.Add($"{item.Key}:\n  Name: {item.Value.Name}\n  Armor Type: {item.Value.GetArmorTypes().ToString()}\n  " +
                                   $"Level Req: {item.Value.RequiredLevel}\n  Item Attributes: Strength: {item.Value.ItemAttributes.Strength}," +
                                   $" Dexterity: {item.Value.ItemAttributes.Dexterity}, " +
                                   $"Intelligence: {item.Value.ItemAttributes.Intelligence}" );
                }
            }

            return returnList;
        }
    }
}
