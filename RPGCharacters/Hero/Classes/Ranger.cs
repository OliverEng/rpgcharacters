﻿using RPGCharacters.Items;
using RPGCharacters.Items.Exceptions;

namespace RPGCharacters.Hero.Classes
{
    public class Ranger : Hero
    {
        // Properties
        private readonly List<Armor.ArmorTypes> _allowedArmorTypesList = new List<Armor.ArmorTypes>()
        {
            Armor.ArmorTypes.Leather,
            Armor.ArmorTypes.Mail
        };

        private readonly List<Weapon.WeaponTypes> _allowedWeaponTypesList = new List<Weapon.WeaponTypes>()
        {
            Weapon.WeaponTypes.Bow
        };

        // Constructor
        public Ranger(string name) : base(name)
        {
            SetStats(1, 7, 1);
        }

        /// <inheritdoc />
        public override void LevelUp()
        {
            base.LevelUp();
            AddStats(1, 5, 1);
        }

        /// <returns>base method</returns>
        /// <inheritdoc />
        public override string EquipWeapon(Weapon? weaponToEquip)
        {
            if (weaponToEquip != null && !_allowedWeaponTypesList.Contains(weaponToEquip.WeaponType))
            {
                throw new InvalidWeaponException("Ranger can't equip this weapon type");
            }
            return base.EquipWeapon(weaponToEquip);
        }

        /// <returns>base method</returns>
        /// <inheritdoc />
        public override string EquipArmor(Armor? armorToEquip)
        {
            if (armorToEquip != null && !_allowedArmorTypesList.Contains(armorToEquip.ArmorType))
            {
                throw new InvalidArmorException("Ranger can't equip this armor type");
            }
            return base.EquipArmor(armorToEquip);
        }

        /// <inheritdoc />
        public override double CalculateDamage()
        {
            var totalStats = CalculateStats();
            var primaryStat = totalStats.Dexterity;
            if (!EquippedItems.ContainsKey(Item.ItemSlots.Weapon))
            {
                return 1 + primaryStat / 100;
            }
            var weaponDamage = EquippedItems[Item.ItemSlots.Weapon]!.GetWeaponDamage();
            return weaponDamage * (1 + primaryStat / 100);
        }
    }

}
