﻿using RPGCharacters.Items;
using RPGCharacters.Items.Exceptions;
using RPGCharacters.Types;

namespace RPGCharacters.Hero.Classes
{

    public class Mage : Hero
    {
        // Properties
        private readonly List<Armor.ArmorTypes> _allowedArmorTypesList = new List<Armor.ArmorTypes>()
        {
            Armor.ArmorTypes.Cloth,
        };

        private readonly List<Weapon.WeaponTypes> _allowedWeaponTypesList = new List<Weapon.WeaponTypes>()
        {
            Weapon.WeaponTypes.Staff,
            Weapon.WeaponTypes.Wand
        };

        // Constructor
        public Mage(string name) : base(name)
        {
            SetStats(1, 1, 8);
        }

        /// <inheritdoc />
        public override void LevelUp()
        {
            base.LevelUp();
            AddStats(1, 1, 5);
        }

        /// <returns>base method</returns>
        /// <inheritdoc />
        public override string EquipWeapon(Weapon? weaponToEquip)
        {
            if (weaponToEquip != null && !_allowedWeaponTypesList.Contains(weaponToEquip.WeaponType))
            {
                throw new InvalidWeaponException("Mage can't equip this weapon type");
            }
            return base.EquipWeapon(weaponToEquip);
        }

        /// <returns>base method</returns>
        /// <inheritdoc />
        public override string EquipArmor(Armor? armorToEquip)
        {
            if (armorToEquip != null && !_allowedArmorTypesList.Contains(armorToEquip.ArmorType))
            {
                throw new InvalidArmorException("Mage can't equip this armor type");
            }
            return base.EquipArmor(armorToEquip);
        }
        /// <inheritdoc />
        public override double CalculateDamage()
        {
            var totalStats = CalculateStats();
            var primaryStat = totalStats.Intelligence;
            if (!EquippedItems.ContainsKey(Item.ItemSlots.Weapon))
            {
                return 1 + primaryStat / 100;
            }
            var weaponDamage = EquippedItems[Item.ItemSlots.Weapon]!.GetWeaponDamage();
            return weaponDamage * (1 + primaryStat / 100);
        }
    }
}
