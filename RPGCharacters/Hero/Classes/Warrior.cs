﻿using RPGCharacters.Items;
using RPGCharacters.Items.Exceptions;

namespace RPGCharacters.Hero.Classes
{
    public class Warrior : Hero
    {
        // Properties
        private readonly List<Armor.ArmorTypes> _allowedArmorTypesList = new List<Armor.ArmorTypes>()
        {
            Armor.ArmorTypes.Mail,
            Armor.ArmorTypes.Plate
        };

        private readonly List<Weapon.WeaponTypes> _allowedWeaponTypesList = new List<Weapon.WeaponTypes>()
        {
            Weapon.WeaponTypes.Axe,
            Weapon.WeaponTypes.Hammer,
            Weapon.WeaponTypes.Sword
        };

        // Constructor
        public Warrior(string name) : base(name)
        {
            SetStats(5, 2, 1);
        }

        /// <inheritdoc />
        public override void LevelUp()
        {
            base.LevelUp();
            AddStats(3, 2, 1);
        }

        /// <inheritdoc />
        public override string EquipWeapon(Weapon? weaponToEquip)
        {
            if (weaponToEquip != null && !_allowedWeaponTypesList.Contains(weaponToEquip.WeaponType))
            {
                throw new InvalidWeaponException("Warrior can't equip this weapon type");
            }
            return base.EquipWeapon(weaponToEquip);
        }
        /// <inheritdoc />
        public override string EquipArmor(Armor? armorToEquip)
        {
            if (armorToEquip != null && !_allowedArmorTypesList.Contains(armorToEquip.ArmorType))
            {
                throw new InvalidArmorException("Warrior can't equip this armor type");
            }
            return base.EquipArmor(armorToEquip);
        }

        /// <inheritdoc />
        public override double CalculateDamage()
        {
            var totalStats = CalculateStats();
            var primaryStat = totalStats.Strength;
            if (!EquippedItems.ContainsKey(Item.ItemSlots.Weapon))
            {
                return 1 + primaryStat / 100;
            }
            var weaponDamage = EquippedItems[Item.ItemSlots.Weapon]!.GetWeaponDamage();
            return weaponDamage * (1 + primaryStat / 100);
        }
    }
}
