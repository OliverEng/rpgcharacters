﻿using RPGCharacters.Types;

namespace RPGCharacters.Items
{
    public class Armor : Item
    {
        public enum ArmorTypes
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
        public ArmorTypes ArmorType { get; set; }

        public Armor()
        {
            ItemAttributes = new PrimaryAttributes();
        }

        public override ArmorTypes? GetArmorTypes()
        {
            return ArmorType;
        }
    }
}
