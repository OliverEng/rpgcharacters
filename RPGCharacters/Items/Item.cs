﻿using RPGCharacters.Types;

namespace RPGCharacters.Items
{
    public abstract class Item
    {
        public enum ItemSlots
        {
            Head,
            Body,
            Legs,
            Weapon
        }
        public string? Name { get; set; }
        public int RequiredLevel { get; set; }
        public ItemSlots ItemSlot { get; set; }

        public PrimaryAttributes? ItemAttributes { get; set; }

        public WeaponAttributes? WeaponAttributes { get; set; }

        public virtual double GetWeaponDamage()
        {
            return 0;
        }

        public virtual Weapon.WeaponTypes? GetWeaponTypes()
        {
            return null;
        }

        public virtual Armor.ArmorTypes? GetArmorTypes()
        {
            return null;
        }

        /// <summary>
        /// Generates a start weapon for the suitable class
        /// </summary>
        /// <param name="className"></param>
        /// <returns>Generated weapon</returns>
        public static Weapon? GenerateStartWeapon(string className)
        {
            switch (className)
            {
                case "Mage":
                    return new Weapon()
                    {
                        Name = "Common Staff",
                        RequiredLevel = 1,
                        ItemSlot = ItemSlots.Weapon,
                        WeaponType = Weapon.WeaponTypes.Staff,
                        WeaponAttributes = new WeaponAttributes() { Damage = 1, AttackSpeed = 1.1 }
                    };
                case "Ranger":
                    return new Weapon()
                    {
                        Name = "Common Bow",
                        RequiredLevel = 1,
                        ItemSlot = ItemSlots.Weapon,
                        WeaponType = Weapon.WeaponTypes.Bow,
                        WeaponAttributes = new WeaponAttributes() { Damage = 1, AttackSpeed = 1.1 }
                    };
                case "Rogue":
                    return new Weapon()
                    {
                        Name = "Common Dagger",
                        RequiredLevel = 1,
                        ItemSlot = ItemSlots.Weapon,
                        WeaponType = Weapon.WeaponTypes.Dagger,
                        WeaponAttributes = new WeaponAttributes() { Damage = 1, AttackSpeed = 1.1 }
                    };
                case "Warrior":
                    return new Weapon()
                    {
                        Name = "Common Axe",
                        RequiredLevel = 1,
                        ItemSlot = ItemSlots.Weapon,
                        WeaponType = Weapon.WeaponTypes.Axe,
                        WeaponAttributes = new WeaponAttributes() { Damage = 1, AttackSpeed = 1.1 }
                    };
                default:
                    break;
            }

            return null;
        }

        /// <summary>
        /// Generates start body armor for the suitable class
        /// </summary>
        /// <param name="className"></param>
        /// <returns>Generated armor</returns>
        public static Armor? GenerateStartArmor(string className)
        {
            switch (className)
            {
                case "Mage":
                    return new Armor()
                    {
                        Name = "Common Cloth Cloak",
                        RequiredLevel = 1,
                        ItemSlot = ItemSlots.Body,
                        ArmorType = Armor.ArmorTypes.Cloth,
                        ItemAttributes = new PrimaryAttributes() { Intelligence = 5 }
                    };
                case "Ranger":
                    return new Armor()
                    {
                        Name = "Common Leather Vest",
                        RequiredLevel = 1,
                        ItemSlot = ItemSlots.Body,
                        ArmorType = Armor.ArmorTypes.Leather,
                        ItemAttributes = new PrimaryAttributes() { Dexterity = 5 }
                    };

                case "Rogue":
                    return new Armor()
                    {
                        Name = "Common Leather Cloak",
                        RequiredLevel = 1,
                        ItemSlot = ItemSlots.Body,
                        ArmorType = Armor.ArmorTypes.Cloth,
                        ItemAttributes = new PrimaryAttributes() { Dexterity = 5 }
                    };

                case "Warrior":
                    return new Armor()
                    {
                        Name = "Common Plate Armor",
                        RequiredLevel = 1,
                        ItemSlot = ItemSlots.Body,
                        ArmorType = Armor.ArmorTypes.Plate,
                        ItemAttributes = new PrimaryAttributes() { Strength = 5 }
                    };

                default:
                    break;
            }

            return null;
        }
    }
}
