﻿namespace RPGCharacters.Items.Exceptions
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string? message) : base(message)
        {
        }

        public override string Message => base.Message;
    }
}
