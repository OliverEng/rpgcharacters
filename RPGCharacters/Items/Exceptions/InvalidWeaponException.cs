﻿namespace RPGCharacters.Items.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(string? message) : base(message)
        {
        }

        public override string Message => base.Message;
    }
}
