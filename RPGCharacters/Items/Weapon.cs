﻿using RPGCharacters.Types;

namespace RPGCharacters.Items
{
    public class Weapon : Item
    {
        public enum WeaponTypes
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }

        public WeaponTypes WeaponType { get; set; }

        public Weapon()
        {
            WeaponAttributes = new WeaponAttributes();
        }

        public override WeaponTypes? GetWeaponTypes()
        {
            return WeaponType;
        }

        public override double GetWeaponDamage()
        {
            return WeaponAttributes!.Damage * WeaponAttributes.AttackSpeed;
        }

    }
}
