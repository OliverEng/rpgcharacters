﻿namespace RPGCharacters.Types
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; } = 0;
        public int Dexterity { get; set; } = 0;
        public int Intelligence { get; set; } = 0;

        public static PrimaryAttributes operator +(PrimaryAttributes a, PrimaryAttributes b)
        {
            return new PrimaryAttributes()
            {
                Strength = a.Strength + b.Strength,
                Dexterity = a.Dexterity + b.Dexterity,
                Intelligence = a.Intelligence + b.Intelligence
            };
        }
    }
}
