﻿namespace RPGCharacters.Types
{
    public class WeaponAttributes
    {
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
        
    }
}
