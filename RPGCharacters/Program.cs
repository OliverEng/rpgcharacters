﻿using RPGCharacters.Hero;
using RPGCharacters.Hero.Classes;
using RPGCharacters.Items;
using RPGCharacters.Types;
using System.Text.RegularExpressions;
using static RPGCharacters.Items.Weapon;

// Prepare properties
var characterName = "Hero Name";
var className = "";
var invalid = true;

// Introduction message
Console.WriteLine("Welcome to RPGCharacters!\nPlease enter your desired character name:");

// Get name of character from player
GetCharacterName(ref characterName, ref invalid);

// Get class of character from player
var hero = GetHeroClass(ref invalid, ref characterName!, ref className);

// Equip Starter Gear
hero!.EquipWeapon(weaponToEquip: Item.GenerateStartWeapon(className));
hero!.EquipArmor(armorToEquip: Item.GenerateStartArmor(className));

// Display character information
DisplayCharacterInfo(className, hero);
Console.WriteLine("\n Do you wish to display equipped items? y/any");
var display = Console.ReadLine();
if (display == "y")
{
    DisplayEquippedItemsInfo(hero);
}

// Continue to show options..
ShowOptions(hero, className);


// METHODS BELOW!

static void GetCharacterName(ref string? characterName, ref bool invalid)
{
    while (invalid) // Keep trying to get character name...
    {
        characterName = Console.ReadLine();
        if (characterName?.Length is < 2 or > 20)
        {
            Console.WriteLine("Character name must be at least 3 character long and less than 21 character long");
        }
        if (Regex.IsMatch(characterName!, @"^[a-zA-Z _]+$"))
        {
            invalid = false;
        }
        else
        {
            Console.WriteLine("Character name can only contain letters and whitespace");
        }
    }
    invalid = true;
}

static Hero? GetHeroClass(ref bool invalid, ref string characterName, ref string className)
{
    Console.WriteLine($"Welcome, {characterName}. Please select your class by inputting the corresponding number:\n\n 1: Mage\n 2: Ranger\n 3: Rogue\n 4: Warrior");
    while (invalid) // Keep trying to get character class return class..
    {
        var classSelection = Console.ReadLine();
        switch (classSelection)
        {
            case "1":
                Console.WriteLine("\nYou have selected to play as a mage.");
                invalid = false;
                className = "Mage";
                return new Mage(characterName);
            case "2":
                Console.WriteLine("\nYou have selected to play as a ranger.");
                invalid = false;
                className = "Ranger";
                return new Ranger(characterName);
            case "3":
                Console.WriteLine("\nYou have selected to play as a rogue.");
                invalid = false;
                className = "Rogue";
                return new Rogue(characterName);
            case "4":
                Console.WriteLine("\nYou have selected to play as a warrior.");
                invalid = false;
                className = "Warrior";
                return new Warrior(characterName);
            default:
                Console.WriteLine("\nEnter 1 for mage, 2 for ranger, 3 for rogue, 4 for warrior.");
                break;
        }
    }
    invalid = true;
    return null;
}

static void ShowOptions(Hero hero, string className)
{
    while (true) // Keep showing the different options players can make...
    {
        Console.WriteLine("\nYou have a few choices:\n 1: To level-Up\n 2: To show character information\n 3: To display equipped items\n 4: To create and equip a new item");
        var choice = Console.ReadLine();
        switch (choice)
        {
            case "1":
                hero.LevelUp();
                Console.WriteLine($"\nCongratulations, you are now level {hero.GetLevel()}");
                break;
            case "2":
                DisplayCharacterInfo(className, hero);
                break;
            case "3":
                DisplayEquippedItemsInfo(hero);
                break;
            case "4":
                CreateItem(hero);
                break;
            default:
                break;
        }
    }
}

static void DisplayCharacterInfo(string className, Hero hero)
{
    Console.WriteLine($"\nDisplaying your character information:");
    // Loop through hero info from method and display
    var heroInfo = hero!.DisplayInformation(className);
    foreach (var line in heroInfo)
    {
        Console.WriteLine(" " + line);
    }
}

static void DisplayEquippedItemsInfo(Hero? hero)
{
    Console.WriteLine($"\nDisplaying equipped items:");
    // Loop through equipped items from method and display
    var equippedItemInfo = hero!.DisplayItemInformation();
    foreach (var line in equippedItemInfo)
    {
        Console.WriteLine(" " + line + "\n");
    }
}

static void CreateItem(Hero? hero)
{
    Console.WriteLine("\nWhat type of item would you like to create?\n 1: Weapon\n 2. Armor");
    var input = Console.ReadLine();
    var valid = false;
    while (!valid) // Keep trying to get valid input
    {
        if (input is "1" or "2")
        {
            valid = true;
        }
        else
        {
            input = Console.ReadLine();
        }
    }
    switch (input) // Run create weapon or create armor method depending on input.
    {
        case "1":
            CreateWeapon(hero);
            break;


        case "2":
            CreateArmor(hero);
            break;
    }
}

static void CreateWeapon(Hero? hero)
{
    // Prepare variables
    var weaponType = WeaponTypes.Axe;
    var weaponName = "";
    var weaponDamage = 0.0;
    var weaponAttackSpeed = 0.0;
    var weaponRequiredLevel = 0;

    Console.WriteLine("\nWhat type of weapon do you want to create?\n 1: Axe\n 2: Bow\n 3: Dagger\n 4: Hammer\n 5: Staff\n 6: Sword\n 7: Wand");
    var input = Console.ReadLine();
    var valid = false;
    while (!valid) // Keep trying to get selected weapon type selection
    {
        if (Regex.IsMatch(input!, @"^[1-7]$"))
        {
            valid = true;
        }
        else
        {
            input = Console.ReadLine();
        }
    }

    // Assign weapon type depending on input
    weaponType = input switch
    {
        "1" => WeaponTypes.Axe,
        "2" => WeaponTypes.Bow,
        "3" => WeaponTypes.Dagger,
        "4" => WeaponTypes.Hammer,
        "5" => WeaponTypes.Staff,
        "6" => WeaponTypes.Sword,
        "7" => WeaponTypes.Wand,
        _ => weaponType
    };
    var invalid = true;
    while (invalid) // Keep trying to get weapon name
    {
        Console.WriteLine("\nWhat do you want to name your weapon?");
        weaponName = Console.ReadLine();
        if (weaponName?.Length is < 2 or > 31)
        {
            Console.WriteLine("Weapon name must be at least 3 character long and less than 31 character long");
        }
        else if (Regex.IsMatch(weaponName!, @"^[a-zA-Z _]+$"))
        {
            invalid = false;
        }
        else
        {
            Console.WriteLine("Weapon name can only contain letters and whitespace");
        }
    }

    Console.WriteLine("\nHow much damage should your weapon do?");
    valid = false;
    while (!valid) // Keep trying to get damage
    {
        input = Console.ReadLine();
        // Try to convert input from string to float
        valid = double.TryParse(input, out weaponDamage);
        if (Regex.IsMatch(input!, @"[0-9]+[.[0-9]+]?"))
        {
            valid = true;
        }
        else
        {
            Console.WriteLine("Damage must be an int or double");
            valid = false;
        }
    }


    Console.WriteLine("\nWhat attack speed should your weapon have?");
    valid = false;
    while (!valid) // Keep trying to get weapon attack speed
    {
        input = Console.ReadLine();
        // Try to convert input from string to float
        valid = double.TryParse(input, out weaponAttackSpeed);
        if (Regex.IsMatch(input!, @"[0-9]+[.[0-9]+]?"))
        {
            valid = true;
        }
        else
        {
            Console.WriteLine("Attack Speed must be an int or double");
            valid = false;
        }
    }

    Console.WriteLine("\nWhat level should be required to equip the weapon?");
    valid = false;
    while (!valid) // Keep trying to get required level
    {
        input = Console.ReadLine();
        // Try to convert input from string to int
        valid = int.TryParse(input, out weaponRequiredLevel);
        if (Regex.IsMatch(input!, @"^[0-9]+"))
        {
            valid = true;
        }
        else
        {
            Console.WriteLine("Required Level must be an int.");
            valid = false;
        }
    }

    // Create new weapon with values from inputs
    var newWeapon = new Weapon()
    {
        Name = weaponName,
        RequiredLevel = weaponRequiredLevel,
        ItemSlot = Item.ItemSlots.Weapon,
        WeaponType = weaponType,
        WeaponAttributes = new WeaponAttributes() { Damage = weaponDamage, AttackSpeed = weaponAttackSpeed }
    };

    // Try to equip weapon
    try
    {
        Console.WriteLine("\n" + hero!.EquipWeapon(newWeapon));
    }
    catch (Exception e)
    {
        Console.WriteLine("\n" + e.Message);
    }
}

static void CreateArmor(Hero? hero)
{
    // Prepare variables
    var armorSlot = Item.ItemSlots.Body;
    var armorType = Armor.ArmorTypes.Cloth;
    var armorName = "";
    var armorRequiredLevel = 0;
    var armorStrengthModifier = 0;
    var armorDexterityModifier = 0;
    var armorIntelligenceModifier = 0;

    Console.WriteLine("\nWhat type of armor do you want to create?\n 1: Cloth\n 2: Leather\n 3: Mail\n 4: Plate");
    var input = Console.ReadLine();
    var valid = false;
    while (!valid) // Keep trying to get armor type selection
    {
        if (Regex.IsMatch(input!, @"^[1-4]$"))
        {
            valid = true;
        }
        else
        {
            input = Console.ReadLine();
        }
    }

    // Assign armor type based on input
    armorType = input switch
    {
        "1" => Armor.ArmorTypes.Cloth,
        "2" => Armor.ArmorTypes.Leather,
        "3" => Armor.ArmorTypes.Mail,
        "4" => Armor.ArmorTypes.Plate,
        _ => armorType
    };

    Console.WriteLine("\nWhere do you want to equip your armor?\n 1: Head\n 2: Body\n 3: Legs");
    input = Console.ReadLine();
    valid = false;
    while (!valid) // Keep trying to get armor slot selection
    {
        if (Regex.IsMatch(input!, @"^[1-3]$"))
        {
            valid = true;
        }
        else
        {
            input = Console.ReadLine();
        }
    }

    // Assign item slot based on input
    armorSlot = input switch
    {
        "1" => Item.ItemSlots.Head,
        "2" => Item.ItemSlots.Body,
        "3" => Item.ItemSlots.Legs,
        _ => armorSlot
    };

    var invalid = true;
    while (invalid) // Keep trying to get armor name
    {
        Console.WriteLine("\nWhat do you want to name your armor");
        armorName = Console.ReadLine();
        if (armorName?.Length is < 2 or > 31)
        {
            Console.WriteLine("Armor name must be at least 3 character long and less than 31 character long");
        }
        else if (Regex.IsMatch(armorName!, @"^[a-zA-Z _]+$"))
        {
            invalid = false;
        }
        else
        {
            Console.WriteLine("Armor name can only contain letters and whitespace");
        }
    }

    Console.WriteLine("\nWhat strength modifier should your armor have?");
    valid = false;
    while (!valid) // Keep trying to get strength modifier
    {
        input = Console.ReadLine();
        // Try to convert input from string to int
        valid = int.TryParse(input, out armorStrengthModifier);
        if (Regex.IsMatch(input!, @"^[0-9]+"))
        {
            valid = true;
        }
        else
        {
            Console.WriteLine("Modifier must be an int");
            valid = false;
        }
    }

    Console.WriteLine("\nWhat dexterity modifier should your armor have?");
    valid = false;
    while (!valid) // Keep trying to get dexterity modifier
    {
        input = Console.ReadLine();
        // Convert input from string to int
        valid = int.TryParse(input, out armorDexterityModifier);
        if (Regex.IsMatch(input!, @"^[0-9]+"))
        {
            valid = true;
        }
        else
        {
            Console.WriteLine("Modifier must be an int");
            valid = false;
        }
    }

    Console.WriteLine("\nWhat intelligence modifier should your armor have?");
    valid = false;
    while (!valid) // Keep trying to get intelligence modifier
    {
        input = Console.ReadLine();
        // Convert input from string to int
        valid = int.TryParse(input, out armorIntelligenceModifier);
        if (Regex.IsMatch(input!, @"^[0-9]+"))
        {
            valid = true;
        }
        else
        {
            Console.WriteLine("Modifier must be an int");
            valid = false;
        }
    }

    Console.WriteLine("\nWhat level should be required to equip the armor?");
    valid = false;
    while (!valid) // Keep trying to required item level
    {
        input = Console.ReadLine();
        // Convert input from string to int
        valid = int.TryParse(input, out armorRequiredLevel);
        if (Regex.IsMatch(input!, @"^[0-9]+"))
        {
            valid = true;
        }
        else
        {
            Console.WriteLine("Required Level must be an int.");
            valid = false;
        }
    }

    // Create armor based on inputs
    var newArmor = new Armor()
    {
        Name = armorName,
        RequiredLevel = armorRequiredLevel,
        ItemSlot = armorSlot,
        ArmorType = armorType,
        ItemAttributes = new PrimaryAttributes() { Strength = armorStrengthModifier, Dexterity = armorDexterityModifier, Intelligence = armorIntelligenceModifier }
    };
    // Try to equip armor
    try
    {
        Console.WriteLine("\n" + hero!.EquipArmor(newArmor));
    }
    catch (Exception e)
    {
        Console.WriteLine("\n" + e.Message);
    }
}